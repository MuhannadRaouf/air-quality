const axios = require("axios");
const { expect } = require("chai");
const { describe, it } = require("mocha");

const baseURL = "http://localhost:3000";
const cordinates = { lon: 2.351666, lat: 48.856613 };

describe("Air Quality Testing", () => {
  it("GET Most polluted time in Paris, It should respond with 200", async () => {
    const data = await axios.get(`${baseURL}/air-quality/most-polluted`, {
      params: {
        city: "Paris",
      },
    });
    expect(data.status).equal(200);
  });

  it("GET Air quality for nearest city, It should respond with 200", async () => {
    const data = await axios.get(`${baseURL}/air-quality/pollution`, {
      params: {
        lat: cordinates.lat,
        lon: cordinates.lon,
      },
    });
    expect(data.status).equal(200);
  });
});
