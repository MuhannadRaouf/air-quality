const swaggerJSDoc = require("swagger-jsdoc");

const BadRequestErrorResponse = {
  description: 'Failed',
  content: {
    'application/json': {
      schema: {
        type: 'object',
        properties: {
          error: {
            type: 'string',
            example: 'Something went wrong',
          },
        },
      },
    },
  },
};

const options = {
  swaggerDefinition: {
    // Path to the API specification
    openapi: "3.0.3",
    info: {
      title: "Air Quality",
      version: "1.0.0",
      description: "API documentation for Air Quality app to check the nearest city pollution",
    },
    servers: [
      {
        url: "http://localhost:3000/air-quality", // Replace with your server URL
      },
    ],
    tags: [
      {
        name: 'Pollution',
        description: 'Everything about the Pollution APIs',
      },
    ],
    paths: {
      '/pollution': {
        get: {
          tags: [
            'Pollution',
          ],
          summary: 'Get nearest city or cordinates pollution',
          parameters: [
            {
              name: 'lat',
              in: 'query',
              description: 'The latitude cordinates',
              schema: {
                type: 'number',
                default: null,
              },
            },
            {
              name: 'lon',
              in: 'query',
              description: 'The longitude cordinates',
              schema: {
                type: 'number',
                default: null,
              },
            },
          ],
          responses: {
            200: {
              description: 'Ok',
              content: {
                'application/json': {
                  schema: {
                    type: 'object',
                    properties: {
                      Result: {
                        type: 'object',
                        properties: {
                          ts: {
                            type: 'string',
                            format: 'date-time',
                            example: new Date(),
                            description: 'Time for the pollution request',
                          },
                          aqius: {
                            type: 'number',
                            example: 53,
                            description: 'Pollutant details appropriate US AQIs',
                          },
                          mainus: {
                            type: 'string',
                            example: 'p2',
                            description: 'Main pollutant for US AQI',
                          },
                          aqicn: {
                            type: 'number',
                            example: 25,
                            description: 'Pollutant details appropriate Chinese AQIs',
                          },
                          maincn: {
                            type: 'string',
                            example: 'p1',
                            description: 'Main pollutant for Chinese AQI',
                          },
                          city: {
                            type: 'string',
                            example: 'Paris',
                            description: 'The city name of the pollution request',
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
            400: BadRequestErrorResponse,
          },
        },
      },
      '/most-polluted': {
        get: {
          tags: [
            'Pollution',
          ],
          summary: 'Get most polluted time for Paris or any other city',
          parameters: [
            {
              name: 'city',
              in: 'query',
              description: 'The city name to get the most polluted time for it',
              schema: {
                type: 'string',
                example: 'Paris',
                default: null,
              },
            },
          ],
          responses: {
            200: {
              description: 'Success',
              content: {
                'application/json': {
                  schema: {
                    type: 'object',
                    properties: {
                      Result: {
                        type: 'object',
                        properties: {
                          most_polluted_datetime: {
                            type: 'string',
                            format: 'date-time',
                            example: new Date(),
                            description: 'Most polluted time for this city',
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
            400: BadRequestErrorResponse,
          },
        },
      }
    },
  },
  apis: []
  
};

const swaggerSpec = swaggerJSDoc(options);

module.exports = swaggerSpec;
