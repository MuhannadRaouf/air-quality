# Air Quality App
This is a REST API application thats responsible for exposing “the air quality information” of a nearest city to GPS coordinates using iqair: https://www.iqair.com/fr/commercial/air-quality-monitors/airvisual-platform/api.

## Table of Contents
* Before You Begin
* Installation
* Usage
* Testing
* CronJob
* API Documentation

## Before You Begin

Before you start using this Node.js application, make sure you have the following prerequisites:

1. **Node.js**: Ensure that you have Node.js installed on your machine. You can download it from the official Node.js website: [https://nodejs.org](https://nodejs.org)

2. **Package Manager**: This application uses npm (Node Package Manager) to manage dependencies. npm is typically installed automatically with Node.js. To verify if npm is installed, run the following command in your terminal:

    ```bash
    npm -v
    ```
## Installation

To install and run the application, follow these steps:

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/MuhannadRaouf/air-quality.git
    ```

2. Navigate to the project directory:

    ```bash
    cd Air-Quality
    ```

3. Install the dependencies:

    ```bash
    npm install
    ```

4. Start the application:

    ```bash
    npm start
    ```

The application will be running at http://localhost:3000.

## Usage
To use this app you just need to access the Swagger Docs. (see API Documentation section).

## Testing

To run the unit testing for this application, follow these steps:

1. Start the application:

    ```bash
    npm start
    ```
2. In a new terminal run this:

    ```bash
    npm test
    ```
## CronJob

The cronJob implemented to check "air quality" for the Paris zone ( latitude: 48.856613 ,longitude: 2.352222) every 1 minute then save them in the MongoDB database

## API Documentation

The API documentation is available at http://localhost:3000/docs.
This documentation provides detailed information about the available API endpoints, request/response formats, and any additional details specific to your API.