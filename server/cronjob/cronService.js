const CronJob = require("node-cron");
const AirQualityModel = require("../air_quality/models/airQualityModel");

const CronService = {};

CronService.savePollutionJob = async () => {
  CronJob.schedule("* * * * *", async () => {
    try {
      await AirQualityModel.getCityPollution({ lat: 48.856613, lon: 2.351666, saveToDB: true });
      console.log("Pollution saved successfully!");
    } catch (error) {
      console.log(`[CronService][savePollutionJob] error while saving pollution: ${error}`);
    }
  });
};

module.exports = CronService;
