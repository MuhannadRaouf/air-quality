const axios = require("axios");
const PollutionSchema = require("../schemas/pollutionSchema");

const AirQualityModel = {};

AirQualityModel.getCityPollution = async ({ lat, lon, saveToDB }) => {
  try {
    let params = { key: "c362407d-74ea-4e8c-b7c6-723c31106e28" };

    if (lat && lon) {
      params = {
        ...params,
        lat,
        lon,
      };
    }

    const response = await axios.get("https://api.airvisual.com/v2/nearest_city", {
      params,
    });

    if (saveToDB) {
      const cityPollutionData = response.data.data.current.pollution;
      cityPollutionData.city = response.data.data.city;
      PollutionSchema.create(cityPollutionData);
    }

    return response.data.data;
  } catch (error) {
    console.error(`[AirQualityModel][getCityPollution]: ${error}`);
    throw error;
  }
};

AirQualityModel.getMostPolluted = async (city = "Paris") => {
  try {
    const mostPollutedQuery = { city };

    const mostPolluted = await PollutionSchema.findOne(mostPollutedQuery).sort({ aqius: -1, ts: -1 }).exec();

    return mostPolluted;
  } catch (error) {
    console.error(`[AirQualityModel][mostPolluted]: ${error}`);
    throw error;
  }
};

module.exports = AirQualityModel;
