const express = require("express");

const router = express.Router();
const RequestValidator = require("../../common/middlewares/requestValidator");
const AirQualityController = require("./controllers/airQualityController");
const AirQualityValidation = require("./validations/airQualityValidations");

router.get(
  "/pollution",
  RequestValidator.validate(AirQualityValidation.getCityPollution),
  AirQualityController.getCityPollution,
);

router.get(
  "/most-polluted",
  RequestValidator.validate(AirQualityValidation.getMostPolluted),
  AirQualityController.getMostPolluted,
);

module.exports = router;
