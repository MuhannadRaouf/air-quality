const AirQualityModel = require("../models/airQualityModel");

const AirQualityController = {};

AirQualityController.getCityPollution = async (req, res) => {
  try {
    const { lat = null, lon = null } = req.query;

    const cityPollution = await AirQualityModel.getCityPollution({ lat, lon });

    res.status(200).send({ Result: cityPollution.current.pollution });
  } catch (error) {
    console.error(`[AirQualityController] [getCityPollution]: error while getting pollution: ${error}`);
    res.status(400).send({ error: "Something went wrong" });
  }
};

AirQualityController.getMostPolluted = async (req, res) => {
  try {
    const { city } = req.query;
    const mostPolluted = await AirQualityModel.getMostPolluted(city);

    res.status(200).send({ most_polluted_datetime: mostPolluted.ts });
  } catch (error) {
    console.error(`[AirQualityController] [getMostPolluted]: error while getting pollution: ${error}`);
    res.status(400).send({ error: "Something went wrong" });
  }
};

module.exports = AirQualityController;
