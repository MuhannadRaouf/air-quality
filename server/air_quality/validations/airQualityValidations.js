const Joi = require("joi");

const AirQualityValidator = {};

AirQualityValidator.getCityPollution = {
  query: Joi.object()
    .keys({
      lat: Joi.string(),
      lon: Joi.string(),
    })
    .and("lat", "lon"),
};

AirQualityValidator.getMostPolluted = {
  query: Joi.object().keys({
    city: Joi.string(),
  }),
};

module.exports = AirQualityValidator;
