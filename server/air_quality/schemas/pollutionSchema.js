const mongoose = require("mongoose");

const pollutionSchema = new mongoose.Schema({
  city: String,
  latitude: String,
  longitude: String,
  ts: Date,
  aqius: Number,
  mainus: String,
  aqicn: Number,
  maincn: String,
});

const PollutionModel = mongoose.model("Pollution", pollutionSchema);

module.exports = PollutionModel;
