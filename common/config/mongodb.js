const mongoose = require("mongoose");

const config = {
  username: "mohanadraouf95",
  password: "2wddToElSdZDFabc",
  clustername: "airquality-cluster0",
};

const url = `mongodb://${config.username}:${config.password}@ac-ald6lbd-shard-00-00.syagmkn.mongodb.net:27017,ac-ald6lbd-shard-00-01.syagmkn.mongodb.net:27017,ac-ald6lbd-shard-00-02.syagmkn.mongodb.net:27017/airQuality?ssl=true&replicaSet=atlas-ofpigk-shard-0&authSource=admin&retryWrites=true&w=majority`;

mongoose
  .connect(url)
  .then(() => {
    console.log("Connected to database ");
  })
  .catch((err) => {
    console.error(`Error connecting to the database. \n${err}`);
  });

module.exports = mongoose;
