const express = require("express");
const swaggerUi = require("swagger-ui-express");
const swaggerSpec = require("./swagger");

require("./common/config/mongodb");
const airQualityRouter = require("./server/air_quality/index");
const CronService = require("./server/cronjob/cronService");

const app = express();

/** Serve Swagger API documentation **/
app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.use("/air-quality", airQualityRouter);

/** Starting Cron Job Service **/
CronService.savePollutionJob();

/** Start the server **/
app.listen(3000, () => {
  console.log("Started application on port %d", 3000);
});
